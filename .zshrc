export ZSH=/home/dan/.oh-my-zsh

ZSH_THEME="robbyrussell"
HYPHEN_INSENSITIVE="true"
DISABLE_UPDATE_PROMPT="true"

plugins=(gitfast history wd docker zsh-autosuggestions npm sudo httpie terraform)

source $ZSH/oh-my-zsh.sh

# NPM (non-root install)
NPM_PACKAGES="${HOME}/.npm-packages"
PATH="$NPM_PACKAGES/bin:$PATH"
unset MANPATH
export MANPATH="$NPM_PACKAGES/share/man:$(manpath)"

# Golang
export GOPATH="${HOME}/.go"
export PATH=$PATH:/usr/local/go/bin
export PATH="${PATH}:${GOPATH}/bin"

# Android
export ANDROID_HOME="/opt/android-sdk"
export PATH="${PATH}:${ANDROID_HOME}/tools"

# Rust
export PATH="${PATH}:/home/dan/.cargo/bin"

# Git
alias gs='tig --all'
alias gaa='git add -A'
alias gr='git rebase'
alias gra='git rebase --abort'
alias grc='git rebase --continue'
alias gri='git rebase -i'
alias gca='git add -A && git commit'

# Misc
alias aptup="sudo apt update && sudo apt full-upgrade -y && sudo apt autoremove -y"
alias pbcopy='xclip -selection clipboard'
alias ack='ag'
alias dc='docker-compose'
alias global grpe='grep'
alias open="gio open &> /dev/null"
alias diff="git diff --no-index"
alias qr="qrencode -t PNG -s 16 -d 300 -o /tmp/qr.png && xdg-open /tmp/qr.png"

# Bat
export BAT_THEME='Monokai Extended Light'
alias cat=bat
