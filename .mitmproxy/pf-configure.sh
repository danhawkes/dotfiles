#!/bin/bash
sudo sysctl -w net.inet.ip.forwarding=1
sudo pfctl -d
sudo pfctl -f "$(dirname $0)/pf.conf"
sudo pfctl -e
